program tpsvr;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, windows, MainctrlFrm, DataModule
  { you can add units after this };

{$R *.res}
var hMutex : HWND;
begin
  hMutex := CreateMutex(nil,false,'TPS_RUNONCE');
  if WaitForSingleObject(hMutex,0)<>wait_TimeOut then //程序没有被运行过
  begin
    RequireDerivedFormResource:=True;
    Application.Initialize;
    Application.CreateForm(TMainCtrlForm, MainCtrlForm);
    Application.CreateForm(TDM, DM);
    Application.Run;
  end;
end.

