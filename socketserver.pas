unit socketserver;

interface

uses
  Windows;

const iocpsv = 'iocpsv.dll';  
  
type 
    TFnOnReceive = procedure(soc: DWord; buf: PChar; len: DWord); stdcall;
    TFnOnAccept = procedure(soc: DWord); stdcall;
    TFnOnExcept = procedure(str: PChar); stdcall;

    function execServer(nPort: DWord): Boolean; stdcall;
    procedure exitServer(); stdcall;
    procedure setCallbackFnAccept(fun: TFnOnAccept); stdcall;
    procedure setCallbackFnExcept(fun: TFnOnExcept); stdcall;
    procedure setCallbackFnReceive(fun: TFnOnReceive); stdcall;
    function getSocketAddr(socket: DWord): PChar; stdcall;
    procedure addWorkerThread(n: integer); stdcall;
    procedure iocPost(socket: DWord; buffer: PChar; len: DWord); stdcall;

implementation

function execServer(nPort: DWord): Boolean; stdcall; external iocpsv name 'execServer';
procedure exitServer; stdcall; external iocpsv name 'exitServer';
procedure setCallbackFnAccept(fun: TFnOnAccept); stdcall; external iocpsv name 'setCallbackFnAccept';
procedure setCallbackFnExcept(fun: TFnOnExcept); stdcall; external iocpsv name 'setCallbackFnExcept';
procedure setCallbackFnReceive(fun: TFnOnReceive); stdcall; external iocpsv name 'setCallbackFnReceive';
function getSocketAddr(socket: DWord): PChar; stdcall; external iocpsv name 'getSocketAddr';
procedure addWorkerThread(n: integer); stdcall; external iocpsv name 'addWorkerThread';
procedure iocPost(socket: DWord; buffer: PChar; len: DWord); stdcall; external iocpsv name 'iocPost';

end.
