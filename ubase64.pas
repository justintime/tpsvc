unit ubase64;

interface

const
  Base64CharTable: array[0..63] of byte = (
    //'A','B','C','D','E','F','G','H','I','J',
    65, 66, 67, 68, 69, 70, 71, 72, 73, 74,
    //'K','L','M','N','O','P','Q','R','S','T',
    75, 76, 77, 78, 79, 80, 81, 82, 83, 84,
    //'U','V','W','X','Y','Z','a','b','c','d',
    85, 86, 87, 88, 89, 90, 97, 98, 99, 100,
    //'e','f','g','h','i','j','k','l','m','n',
    101, 102, 103, 104, 105, 106, 107, 108, 109, 110,
    //'o','p','q','r','s','t','u','v','w','x',
    111, 112, 113, 114, 115, 116, 117, 118, 119, 120,
    //'y','z','0','1','2','3','4','5','6','7',
    121, 122, 48, 49, 50, 51, 52, 53, 54, 55,
    //'8','9','+','/'
    56, 57, 43, 47
    );
  Base64ByteTable: array[0..127] of byte = (
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 62, 255, 255, 255, 63, 52, 53,
    54, 55, 56, 57, 58, 59, 60, 61, 255, 255,
    255, 255, 255, 255, 255, 0, 1, 2, 3, 4,
    5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
    15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
    25, 255, 255, 255, 255, 255, 255, 26, 27, 28,
    29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
    39, 40, 41, 42, 43, 44, 45, 46, 47, 48,
    49, 50, 51, 255, 255, 255, 255, 255
    );

procedure Base64Decode(srcbuf: Pointer; srclen: dword; dstbuf: Pointer; var dstlen: dword);
procedure Base64Encode(srcbuf: Pointer; srclen: dword; dstbuf: Pointer; var dstlen: dword);

implementation

//=================================================
// srcbuf: BASE64数据
// dstbuf：通过GetMem获取 初始dstlen直接取srclen
// 长度结果保存在 dstlen 中
//=================================================
procedure Base64Decode(srcbuf: Pointer; srclen: dword; dstbuf: Pointer; var dstlen: dword);
var
  i, cnt: integer;
  d1, d2: dword;
  p1, p2: pbyte;
  b: byte;
  function Base64MapCharToByte(c: byte): byte;
  begin
    Result := Base64ByteTable[c];
  end;
begin
  p1 := pbyte(srcbuf);
  p2 := pbyte(dstbuf);
  if srclen < 4 then
  begin
    dstlen := 0;
    exit;
  end;
  cnt := srclen div 4;
  for i := 1 to cnt - 1 do
  begin
    d1 := 0;
    d2 := p1^;
    d1 := d1 or (Base64MapCharToByte((p1^)) shl 26);
    Inc(p1);
    d1 := d1 or (Base64MapCharToByte((p1^)) shl 20);
    Inc(p1);
    d1 := d1 or (Base64MapCharToByte((p1^)) shl 14);
    Inc(p1);
    d1 := d1 or (Base64MapCharToByte((p1^)) shl 8);
    //
    p2^ := d1 shr 24;
    Inc(p2);
    p2^ := (d1 shr 16) and $FF;
    Inc(p2);
    p2^ := (d1 shr 8) and $FF;
    Inc(p1);
    Inc(p2);
  end;
  d1 := 0;
  d2 := p1^;
  b := Base64MapCharToByte((p1^));
  if b < 64 then
    d1 := d1 or (b shl 26);
  Inc(p1);
  b := Base64MapCharToByte((p1^));
  if b < 64 then
    d1 := d1 or (b shl 20);
  Inc(p1);
  b := Base64MapCharToByte((p1^));
  if b < 64 then
    d1 := d1 or (b shl 14);
  Inc(p1);
  b := Base64MapCharToByte((p1^));
  if b < 64 then
    d1 := d1 or (b shl 8);
  p2^ := d1 shr 24;
  Inc(p2);
  p2^ := (d1 shr 16) and $FF;
  Inc(p2);
  p2^ := (d1 shr 8) and $FF;
  dstlen := cnt * 3; // R-Len
end;

//=================================================
// srcbuf: 待处理数据   srclen: 数据长度
// dstbuf：通过 GetMem 获取
// 初始dstlen计算方法
// (Length mod 3) = 0 then dstlen := (i div 3) * 4 + 1 else dstlen := (i div 3) * 4 + 5;
// 或干脆 dstlen := (i div 3) * 4 + 5;
// 长度结果保存在 dstlen 中
//=================================================
procedure Base64Encode(srcbuf: Pointer; srclen: dword; dstbuf: Pointer; var dstlen: dword);
var
  i, cnt: integer;
  d1: dword;
  p1, p2: pbyte;
  b1, b2: byte;
  function Base64MapByteToChar(b: byte): byte;
  begin
    Result := Base64CharTable[b];
  end;
begin
  p1 := pbyte(srcbuf);
  p2 := pbyte(dstbuf);
  if srclen <= 0 then
  begin
    dstlen := 0;
    exit;
  end;
  cnt := srclen div 3;
  for i := 1 to cnt do
  begin
    d1 := 0;
    d1 := d1 or (p1^ shl 24);//重组4*8＝32位字节，先把m移到最高八位。
    Inc(p1);
    d1 := d1 or (p1^ shl 16);//重组4*8＝32位字节，先把m移到第二个八位。
    Inc(p1);
    d1 := d1 or (p1^ shl 8); //重组4*8＝32位字节，先把c移到第三个八位。
    p2^ := Base64MapByteToChar((d1 shr 26) and $3F);
    Inc(p2);                                        //and 3F=011011=27=b
    p2^ := Base64MapByteToChar((d1 shr 20) and $3F); //(d1 shr 20)=011011 010110
    Inc(p2);                                       //and 3F=010110=22=W
    p2^ := Base64MapByteToChar((d1 shr 14) and $3F); //(d1 shr 14)=011011 010110 110101
    Inc(p2);                                       //and 3F=110101=53=1
    p2^ := Base64MapByteToChar((d1 shr 8) and $3F);  //(d1 shr 8)=100011
    Inc(p1);
    Inc(p2);                                     //and 3F=100011=35=j
  end;
  dstlen := cnt * 4; // R-Len
  if (srclen mod 3) = 1 then
  begin
    b1 := p1^;
    Inc(p1);
    p2^ := Base64MapByteToChar(b1 shr 2);
    Inc(p2);
    p2^ := Base64MapByteToChar(((b1 and $3) shl 4) or (0 shr 4));
    Inc(p2);
    p2^ := byte('=');
    Inc(p2);
    p2^ := byte('=');
    Inc(p2);
    dstlen := (cnt + 1) * 4; // R-Len
  end
  else if (srclen mod 3) = 2 then
  begin
    b1 := p1^;
    Inc(p1);
    b2 := p1^;
    Inc(p1);
    p2^ := Base64MapByteToChar(b1 shr 2);
    Inc(p2);
    p2^ := Base64MapByteToChar(((b1 and $3) shl 4) or (b2 shr 4));
    Inc(p2);
    p2^ := Base64MapByteToChar(((b2 and $0F) shl 2) or (0 shr 6));
    Inc(p2);
    p2^ := byte('=');
    Inc(p2);
    dstlen := (cnt + 1) * 4; // R-Len
  end;
end;

end.
